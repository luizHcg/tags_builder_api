﻿using FluentNHibernate.Mapping;
using Model;

namespace DAO.Maps
{
    public class UserMap: ClassMap<User>
    {
        public UserMap()
        {
            Table("UEB_TB_USER");
            Id(c => c.Id).GeneratedBy.Sequence("SEQ_UEB_TB_USER");
            Map(c => c.Email).Length(120).Not.Nullable();
            Map(c => c.Password).Length(60).Not.Nullable();
            Map(c => c.Firstname).Length(50).Not.Nullable();
            Map(c => c.LastName).Length(50).Not.Nullable();
            Map(c => c.Username).Length(60).Not.Nullable();
            References<Profile>(c => c.ProfileFk).Column("profileFk");
        }
    }
}
﻿using FluentNHibernate.Mapping;
using Model;

namespace DAO.Maps
{
    public class ProfileMap : ClassMap<Profile>
    {
        public ProfileMap()
        {
            Table("UEB_TB_PROFILE");
            Id(c => c.Id).GeneratedBy.Sequence("SEQ_UEB_TB_PROFILE");
            Map(c => c.Description).Not.Nullable();
            Map(c => c.TypeProfile).Not.Nullable();
        }
    }
}
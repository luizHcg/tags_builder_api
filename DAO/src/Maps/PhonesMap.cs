﻿using FluentNHibernate.Mapping;
using Model;

namespace DAO.Maps
{
    public class PhonesMap : ClassMap<Phones>
    {
        public PhonesMap()
        {
            Table("UEB_TB_PHONES");
            Id(c => c.Id).GeneratedBy.Sequence("SEQ_UEB_TB_PHONES");
            Map(c => c.Phone).Length(14).Not.Nullable();
            References(c => c.UserFk).Column("userFk");
        }
    }
}
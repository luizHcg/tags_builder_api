﻿using System;
using DAO.Maps;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;

namespace DAO
{
    public class ConnectionFactory
    {
        private static FluentConfiguration CreateSession(string connectionString)
        {
            return Fluently.Configure()
                .Database(OracleManagedDataClientConfiguration
                    .Oracle10
                    .ConnectionString(c => c.Is(connectionString))
                    .ShowSql().FormatSql()
                    .DefaultSchema("SYSTEM"))
                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<UserMap>())
                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<PhonesMap>())
                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<ProfileMap>());
            // .ExposeConfiguration(cfg =>
            // {
            //     var schema = new SchemaExport(cfg);
            //     schema.Drop(true, true);
            //     schema.Create(true, true);
            // });
        }

        public static ISessionFactory GetConnection(string connectionString)
        {
            return CreateSession(connectionString).BuildSessionFactory();
        }
    }
}
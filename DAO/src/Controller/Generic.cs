﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Model;
using NHibernate;
using NHibernate.Linq;

namespace DAO.Controller
{
    public class Generic<T> where T : IBaseClass
    {
        private readonly ISession _session;

        public Generic(ISession session)
        {
            _session = session;
        }

        public async Task<T> SaveOrUpdate(T entity)
        {
            var transaction = _session.BeginTransaction();

            try
            {
                await _session.SaveOrUpdateAsync(entity);
                transaction.Commit();

                return entity;
            }
            catch (Exception err)
            {
                if (!transaction.WasCommitted) transaction.Rollback();
                throw new Exception(err.Message);
            }
            finally
            {
                _session.Close();
                _session.Dispose();
            }
        }

        public async Task<string> Delete(T entity)
        {
            var transaction = _session.BeginTransaction();

            try
            {
                await _session.DeleteAsync(entity);
                transaction.Commit();

                return "Success remove item.";
            }
            catch (Exception err)
            {
                if (!transaction.WasCommitted) transaction.Rollback();
                throw new Exception(err.Message);
            }
        }

        public async Task<List<T>> FindAll()
        {
            return await _session.Query<T>().ToListAsync();
        }

        public async Task<T> FindById(ISession session, T entity)
        {
            return await session.GetAsync<T>(entity.Id);
        }
    }
}
﻿using System;

namespace Model
{
    public class User : IBaseClass
    {
        public virtual int Id { get; set; }

        public virtual string Username { get; set; }
        public virtual string Password { get; set; }
        public virtual string Firstname { get; set; }
        public virtual string LastName { get; set; }
        public virtual string Email { get; set; }
        public virtual Profile ProfileFk { get; set; }
    }
}
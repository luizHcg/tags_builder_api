﻿namespace Model
{
    public class Phones : IBaseClass
    {
        public virtual int Id { get; set; }
        public virtual User UserFk { get; set; }
        public virtual string Phone { get; set; }
    }
}
﻿namespace Model
{
    public class Profile : IBaseClass
    {
        public virtual int Id { get; set; }
        public virtual string Description { get; set; }
        public virtual string TypeProfile { get; set; }
    }
}
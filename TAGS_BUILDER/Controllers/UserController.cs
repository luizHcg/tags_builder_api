﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DAO.Controller;
using Microsoft.AspNetCore.Mvc;
using Model;
using NHibernate;

namespace TAGS_BUILDER.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController
    {
        private readonly ISession _session;

        public UserController(ISession session)
        {
            _session = session;
        }

        [HttpGet]
        public ActionResult<List<User>> Get()
        {
            return new Generic<User>(_session).FindAll().Result;
        }

        [HttpPost]
        public ActionResult<User> PostOrPut([FromBody] User entity)
        {
            entity.Password = (BCrypt.Net.BCrypt.HashPassword(entity.Password, 10));
            return new Generic<User>(_session).SaveOrUpdate(entity).Result;
        }

        [HttpDelete]
        public ActionResult<string> Delete([FromBody] User entity)
        {
            return new Generic<User>(_session).Delete(entity).Result;
        }
    }
}